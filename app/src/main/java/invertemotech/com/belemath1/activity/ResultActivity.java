package invertemotech.com.belemath1.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.util.ArrayList;

import invertemotech.com.belemath1.R;
import invertemotech.com.belemath1.adapter.ResultListAdapter;

public class ResultActivity extends AppCompatActivity {

    ListView resultList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        resultList = (ListView) findViewById(R.id.list);


        ArrayList arrayList = new ArrayList();

        arrayList.add("asd");

        ResultListAdapter userListAdapter = new ResultListAdapter(getApplicationContext(),arrayList);

        resultList.setAdapter(userListAdapter);
    }
}
