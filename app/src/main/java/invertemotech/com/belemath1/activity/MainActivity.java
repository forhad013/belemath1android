package invertemotech.com.belemath1.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;

import invertemotech.com.belemath1.R;
import invertemotech.com.belemath1.adapter.UserListAdapter;

public class MainActivity extends AppCompatActivity {

    ImageView addNewUser;
    ListView userList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addNewUser = (ImageView) findViewById(R.id.addNewUser);
        userList = (ListView) findViewById(R.id.list);


        addNewUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(),AddNewUser.class);
                startActivity(intent);
            }
        });

        ArrayList arrayList = new ArrayList();

        arrayList.add("asd");

        UserListAdapter userListAdapter = new UserListAdapter(getApplicationContext(),arrayList);

        userList.setAdapter(userListAdapter);

        userList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Intent intent = new Intent(getApplicationContext(),QuizListActivity.class);
                startActivity(intent);
                MainActivity.this.overridePendingTransition(R.anim.slide_out_bottom, R.anim.slide_in_top);
            }
        });

    }
}
