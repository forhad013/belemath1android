package invertemotech.com.belemath1.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import invertemotech.com.belemath1.R;


public class UserListAdapter extends BaseAdapter {


    ArrayList<String> userlist;
    Context context;

    public UserListAdapter(Context context,
                           ArrayList<String> userlist) {


        // TODO Auto-generated constructor stub
        this.context = context;

        this.userlist = userlist;


    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return userlist.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View view = convertView;




            view = LayoutInflater.from(context).inflate(
                    R.layout.user_item, parent, false);









        return view;
    }



    public static String getFormattedDateFromTimestamp(long timestampInMilliSeconds)
    {
        Date date = new Date();
        date.setTime(timestampInMilliSeconds);
        String formattedDate=new SimpleDateFormat("MMM d, yyyy hh:mm:ss").format(date);
        return formattedDate;

    }



}