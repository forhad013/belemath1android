package invertemotech.com.belemath1.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import invertemotech.com.belemath1.R;

public class GameActivity extends AppCompatActivity {



    Button backBtn,nextBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        backBtn = (Button) findViewById(R.id.back);
        nextBtn = (Button) findViewById(R.id.next);


        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(),ResultActivity.class);
                startActivity(intent);
            }
        });

    }
}
