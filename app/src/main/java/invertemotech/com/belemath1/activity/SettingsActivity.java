package invertemotech.com.belemath1.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

import invertemotech.com.belemath1.R;
import invertemotech.com.belemath1.adapter.OnSwipeTouchListener;
import invertemotech.com.belemath1.adapter.SlidingImage_Adapter;

public class SettingsActivity extends AppCompatActivity implements View.OnTouchListener{
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private static final Integer[] IMAGES= {
            R.drawable.mode_duration,
            R.drawable.mode_sequence,
            R.drawable.mode_time,
            R.drawable.mode_wrong
    };
    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();
    CirclePageIndicator indicator;
    Button playBtn;
    RelativeLayout mainArea;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        playBtn = (Button) findViewById(R.id.play);
        mainArea = (RelativeLayout) findViewById(R.id.mainArea);

        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(),GameActivity.class);
                startActivity(intent);
            }
        });
        init();


        mainArea.setOnTouchListener(this);


        mainArea.setOnTouchListener(new OnSwipeTouchListener(SettingsActivity.this) {
            public void onSwipeTop() {


                //Toast.makeText(SettingsActivity.this, "top", Toast.LENGTH_SHORT).show();
            }
            public void onSwipeRight() {
                currentPage = mPager.getCurrentItem()-1;

                Log.e("right",currentPage+"");

               // mPager.setCurrentItem(currentPage++, true);
                mPager.setCurrentItem(currentPage);
               // Toast.makeText(SettingsActivity.this, "right", Toast.LENGTH_SHORT).show();
            }
            public void onSwipeLeft() {
                currentPage = mPager.getCurrentItem()+1;

                Log.e("left",currentPage+"");

                // mPager.setCurrentItem(currentPage++, true);
                mPager.setCurrentItem(currentPage);
              //  Toast.makeText(SettingsActivity.this, "left", Toast.LENGTH_SHORT).show();
            }
            public void onSwipeBottom() {
                //Toast.makeText(SettingsActivity.this, "bottom", Toast.LENGTH_SHORT).show();
            }

        });


    }




    private void init() {
        for(int i=0;i<IMAGES.length;i++)
            ImagesArray.add(IMAGES[i]);

        mPager = (ViewPager) findViewById(R.id.pager);


        mPager.setAdapter(new SlidingImage_Adapter(SettingsActivity.this,ImagesArray));


          indicator = (CirclePageIndicator)
                findViewById(R.id.indicator);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
        indicator.setRadius(5 * density);

         NUM_PAGES =IMAGES.length;
//
//        // Auto start of viewpager
//        final Handler handler = new Handler();
//        final Runnable Update = new Runnable() {
//            public void run() {
//                if (currentPage == NUM_PAGES) {
//                    currentPage = 0;
//                }
//                mPager.setCurrentItem(currentPage++, true);
//            }
//        };
//        Timer swipeTimer = new Timer();
//        swipeTimer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                handler.post(Update);
//            }
//        }, 3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {



            }

            @Override
            public void onPageScrollStateChanged(int pos) {
                Log.e("n",NUM_PAGES+"");
                Log.e("c",currentPage+"");

            }
        });

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {

            try {
                currentPage = mPager.getCurrentItem();

                mPager.setCurrentItem(currentPage++, true);
                return true;
            }catch (Exception e){
                return false;
            }
        } else if (event.getAction() == MotionEvent.ACTION_DOWN) {

            try {
                currentPage = mPager.getCurrentItem();

                mPager.setCurrentItem(currentPage++, true);
                return true;
            }catch (Exception e){
                return false;
            }
        } else {
            return false;
        }
    }
}
